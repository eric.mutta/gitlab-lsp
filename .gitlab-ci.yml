workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      variables:
        CI_COMMIT_BRANCH: $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

variables:
  REGISTRY_HOST: 'registry.gitlab.com'
  REGISTRY_GROUP: 'gitlab-org'

stages:
  - test
  - deploy

default:
  # this node version is used by VS Code
  image: node:18.17.0
  tags:
    - gitlab-org
  cache:
    paths:
      - node_modules/

include:
  - template: Jobs/SAST.gitlab-ci.yml
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml

lint_documentation:
  stage: test
  inherit:
    default: false
  image: registry.gitlab.com/gitlab-org/gitlab-docs/lint-markdown:alpine-3.18-vale-2.29.6-markdownlint-0.37.0-markdownlint2-0.10.0
  script:
    # Lint prose
    - vale --minAlertLevel error docs README.md CONTRIBUTING.md
    # Lint Markdown
    - markdownlint --config .markdownlint.yml --ignore CHANGELOG.md 'docs/**/*.md' *.md

lint:
  stage: test
  script:
    - npm ci
    - npm run eslint
    - npm run prettier

lint_commit:
  stage: test
  image: node:16-slim
  script:
    - apt-get update && apt-get install -y git
    - git fetch origin $CI_MERGE_REQUEST_TARGET_BRANCH_NAME $CI_COMMIT_SHA
    - cd scripts/commit-lint && npm ci
    - node lint.js
  rules:
    - if: '$CI_MERGE_REQUEST_IID && $CI_PROJECT_VISIBILITY == "public"' # lint.js script makes an API call without authentication
      when: always

.install and compile:
  script: &install_and_compile
    - npm clean-install
    - npm run compile

.test:
  stage: test
  before_script:
    - apt-get update -qy
    - apt-get install -qy tinyproxy
  artifacts:
    when: always
    reports:
      junit: junit.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage/cobertura-coverage.xml

test-vs-code-node:
  # uses the default (VS Code) version of node same as all other jobs except for the test-pkg-node
  extends: .test
  script:
    - *install_and_compile
    - npm run test:ci
  variables:
    LSP_COMMAND: node
    LSP_ARGS: './out/node/main.js --stdio'

test-pkg-node:
  # this job runs unit tests with the same node version that pkg uses to create the LS binary
  # this job runs integration tests using the linux binary.
  image: node:18.5.0
  extends: .test
  script:
    - *install_and_compile
    - npm run bundle
    - npm run package
    - npm run test:ci
  variables:
    LSP_COMMAND: ./bin/gitlab-lsp-linux-x64
    LSP_ARGS: '--stdio'

test-vs-code-node-bundle:
  extends: .test
  script:
    - *install_and_compile
    - npm run bundle
    - npm run test:ci
  variables:
    LSP_COMMAND: node
    LSP_ARGS: './out/node/main-bundle.js --stdio'

publish:
  stage: deploy
  only:
    - tags
  before_script:
    # Validate that the repository contains a package.json and extract a few values from it.
    - |
      if [[ ! -f package.json ]]; then
        echo "No package.json found! A package.json file is required to publish a package to GitLab's NPM registry."
        echo 'For more information, see https://docs.gitlab.com/ee/user/packages/npm_registry/#creating-a-project'
        exit 1
      fi
    - NPM_PACKAGE_NAME=$(node -p "require('./package.json').name")
    - NPM_PACKAGE_VERSION=$(node -p "require('./package.json').version")
    # Validate that the package name is properly scoped to the project's root namespace.
    # For more information, see https://docs.gitlab.com/ee/user/packages/npm_registry/#package-naming-convention
    - |
      if [[ ! $NPM_PACKAGE_NAME =~ ^@$CI_PROJECT_ROOT_NAMESPACE/ ]]; then
        echo "Invalid package scope! Packages must be scoped in the root namespace of the project, e.g. \"@${CI_PROJECT_ROOT_NAMESPACE}/${CI_PROJECT_NAME}\""
        echo 'For more information, see https://docs.gitlab.com/ee/user/packages/npm_registry/#package-naming-convention'
        exit 1
      fi
  script:
    - npm ci
    - npm run compile
    - npm run bundle
    - npm run package
    - ./scripts/deploy.sh
