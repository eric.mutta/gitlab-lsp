import type { Config } from 'jest';

const config: Config = {
  collectCoverageFrom: ['src/**/*.ts', '!**/node_modules/**'],
  coverageReporters: ['cobertura'],
  coverageDirectory: './coverage',
};

export default config;
