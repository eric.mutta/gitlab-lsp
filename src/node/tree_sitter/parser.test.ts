import { SuggestionContext } from '../../common/suggestion_client';
import { TreeSitterParser } from '../../common/tree_sitter';
import { TREE_SITTER_LANGUAGES } from './languages';
import { DesktopTreeSitterParser } from './parser';

type SuggestionContexts = { completion: SuggestionContext; generation: SuggestionContext };
const SUGGESTION_CONTEXTS: { [key: string]: SuggestionContexts } = {
  go: {
    completion: {
      document: {
        prefix: 'function sayHello() {',
        suffix: '}',
        filename: 'completion.go',
      },
    },
    generation: {
      document: {
        prefix: '// Create a function that says hello world.',
        suffix: '',
        filename: 'generation.go',
      },
    },
  },
  javascript: {
    completion: {
      document: {
        prefix: 'function sayHello() {',
        suffix: '}',
        filename: 'completion.js',
      },
    },
    generation: {
      document: {
        prefix: '// Create a function that says hello world.',
        suffix: '',
        filename: 'generation.js',
      },
    },
  },
  python: {
    completion: {
      document: {
        prefix: 'def sayHello():',
        suffix: '',
        filename: 'completion.py',
      },
    },
    generation: {
      document: {
        prefix: '# Create a function that says hello world.',
        suffix: '',
        filename: 'generation.py',
      },
    },
  },
  ruby: {
    completion: {
      document: {
        prefix: 'def sayHello(name)',
        suffix: '}',
        filename: 'completion.rb',
      },
    },
    generation: {
      document: {
        prefix: '# Create a function that says hello world.',
        suffix: '',
        filename: 'generation.rb',
      },
    },
  },
  typescript: {
    completion: {
      document: {
        prefix: 'const sayHello: FunnyFunction = () => {',
        suffix: '}',
        filename: 'completion.ts',
      },
    },
    generation: {
      document: {
        prefix: '// Create a function that says hello world.',
        suffix: '',
        filename: 'generation.ts',
      },
    },
  },
};

const XML_COMPLETION_CONTEXT: SuggestionContext = {
  document: {
    prefix: '<div>',
    suffix: '</div>',
    filename: 'completion.xml',
  },
};

const XML_GENERATION_CONTEXT: SuggestionContext = {
  document: {
    prefix: '<!-- Create a block a few attributes hello world. -->',
    suffix: '',
    filename: 'generation.xml',
  },
};

describe('TreeSitterParser', () => {
  let subject: TreeSitterParser;

  beforeEach(async () => {
    subject = new DesktopTreeSitterParser();
    await subject.init();
  });

  describe('getIntent', () => {
    it('getIntent for an unsupported language returns completion', async () => {
      const actual = await subject.getIntent(XML_COMPLETION_CONTEXT);
      expect(actual).toEqual(undefined);
    });

    it('getIntent for an unsupported language returns generation', async () => {
      const actual = await subject.getIntent(XML_GENERATION_CONTEXT);
      expect(actual).toEqual(undefined);
    });

    Object.keys(SUGGESTION_CONTEXTS).forEach((language) => {
      const contexts = SUGGESTION_CONTEXTS[language];
      it.each`
        context                | intent
        ${contexts.completion} | ${'completion'}
        ${contexts.generation} | ${'generation'}
      `('for $context.document.filename', async ({ context, intent }) => {
        const actual = await subject.getIntent(context);
        expect(actual).toEqual(intent);
      });
    });

    it('has coverage for all supported langauges', () => {
      expect(Object.keys(SUGGESTION_CONTEXTS)).toEqual(
        TREE_SITTER_LANGUAGES.map((language) => language.name),
      );
    });
  });
});
