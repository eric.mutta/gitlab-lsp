import fetch from 'cross-fetch';
import { getProxySettings } from 'get-proxy-settings';
import { ProxyAgent } from 'proxy-agent';
import { IFetch, FetchBase } from '../common/fetch';
import https from 'https';
import http from 'http';
import { log, getErrorFromException } from '../common/log';

const httpsAgent = new https.Agent({
  keepAlive: true,
});

const httpAgent = new http.Agent({
  keepAlive: true,
});

export interface LsRequestInit extends RequestInit {
  agent?: ProxyAgent | https.Agent | http.Agent;
}

/**
 * Wrap fetch to support proxy configurations
 */
export class Fetch extends FetchBase implements IFetch {
  #proxy?: ProxyAgent;
  #initialized: boolean = false;
  #userProxy?: string;

  constructor(userProxy?: string) {
    super();

    this.#userProxy = userProxy;
  }

  async initialize(): Promise<void> {
    const proxy = await getProxySettings();

    // Set http_proxy and https_proxy environment variables
    // which will then get picked up by the ProxyAgent and
    // used.

    if (proxy?.http) {
      process.env.http_proxy = `${proxy.http.protocol}://${proxy.http.host}:${proxy.http.port}`;
    }
    if (proxy?.https) {
      process.env.https_proxy = `${proxy.https.protocol}://${proxy.https.host}:${proxy.https.port}`;
    }
    if (this.#userProxy) {
      process.env.http_proxy = this.#userProxy;
      process.env.https_proxy = this.#userProxy;
    }

    if (process.env.http_proxy || process.env.https_proxy) {
      this.#proxy = new ProxyAgent();
      log.info(`Setting proxy to '${process.env.https_proxy || process.env.http_proxy}'`);
    }

    this.#initialized = true;
  }

  async destroy(): Promise<void> {
    this.#proxy?.destroy();
  }

  async fetch(input: RequestInfo | URL, init?: RequestInit): Promise<Response> {
    if (!this.#initialized) {
      throw new Error('LsFetch not initialized. Make sure LsFetch.initialized() was called.');
    }

    if (this.#proxy) {
      if (!init) {
        init = {};
      }
      (init as LsRequestInit).agent = this.#proxy;
    } else if (input.toString().startsWith('https://')) {
      (init as LsRequestInit).agent = httpsAgent;
    } else {
      log.error(input.toString());
      (init as LsRequestInit).agent = httpAgent;
    }
    return this.fetchLogged(input, init);
  }

  private async fetchLogged(input: RequestInfo | URL, init?: RequestInit): Promise<Response> {
    const start = Date.now();
    const url = this.extractURL(input);

    try {
      const resp = await fetch(input, init);
      const duration = Date.now() - start;

      log.debug(`fetch: request to ${url} returned HTTP ${resp.status} after ${duration} ms`);

      return resp;
    } catch (e) {
      const duration = Date.now() - start;
      log.debug(`fetch: request to ${url} threw an exception after ${duration} ms`);
      log.error(`fetch: request to ${url} failed with:`, getErrorFromException(e));
      throw e;
    }
  }

  private extractURL(input: RequestInfo | URL): string {
    if (input instanceof URL) {
      return input.toString();
    }

    if (typeof input === 'string') {
      return input;
    }

    return input.url;
  }
}
