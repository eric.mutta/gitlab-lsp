import fetch from 'cross-fetch';
import { Fetch } from './fetch';
import { IFetch } from '../common/fetch';

jest.mock('cross-fetch');

describe('LsFetch', () => {
  afterEach(() => {
    (fetch as jest.Mock).mockClear();
  });

  describe('fetch', () => {
    describe('not initialized', () => {
      it('should throw Error', async () => {
        const lsFetch = new Fetch();

        await expect(lsFetch.fetch('https://gitlab.com/')).rejects.toThrowError(
          'LsFetch not initialized. Make sure LsFetch.initialized() was called.',
        );
      });
    });

    describe('request Methods', () => {
      const testCases: [keyof IFetch, string, number][] = [
        ['get', 'GET', 200],
        ['post', 'POST', 500],
        ['put', 'PUT', 404],
        ['delete', 'DELETE', 403],
      ];

      test.each(testCases)(
        'should call cross-fetch fetch for %s',
        async (method, expectedMethod, expectedStatusCode) => {
          const lsFetch = new Fetch();
          await lsFetch.initialize();

          const response = { status: expectedStatusCode, json: jest.fn().mockResolvedValue({}) };
          (fetch as jest.Mock).mockResolvedValueOnce(response);

          await lsFetch[method]('https://gitlab.com/');
          expect(fetch).toHaveBeenCalled();

          const [url, init] = (fetch as jest.Mock).mock.calls[0];
          expect(init.method).toBe(expectedMethod);
          expect(url).toBe('https://gitlab.com/');
        },
      );
    });
  });
});
