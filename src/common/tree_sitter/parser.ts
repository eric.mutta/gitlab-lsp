import Parser = require('web-tree-sitter');
import { SuggestionContext } from '../suggestion_client';
import { TreeSitterLanguage } from './languages';
import { getErrorFromException, log } from '../log';

export enum TreeSitterParserLoadState {
  INIT = 'init',
  ERRORED = 'errored',
  READY = 'ready',
  UNIMPLEMENTED = 'unimplemented',
}

export type Intent = 'completion' | 'generation';

export abstract class TreeSitterParser {
  protected loadState: TreeSitterParserLoadState;
  protected languages: TreeSitterLanguage[];
  protected readonly parsers: Record<string, Parser>;

  abstract init(): Promise<void>;

  constructor({ languages }: { languages: TreeSitterLanguage[] }) {
    this.languages = languages;
    this.loadState = TreeSitterParserLoadState.INIT;
    this.parsers = {};
  }

  async getIntent(context: SuggestionContext): Promise<Intent | undefined> {
    let parser: Parser | undefined;
    try {
      if (this.loadState === TreeSitterParserLoadState.INIT) {
        await this.init();
      }

      parser = await this.findGrammar(context.document.filename);
    } catch (err) {
      log.warn(
        'TreeSitterParser: Error initializing an appropriate tree-sitter parser',
        getErrorFromException(err),
      );
      this.loadState = TreeSitterParserLoadState.ERRORED;
      return;
    }

    let intent: Intent | undefined;
    if (parser) {
      const tree = parser.parse(context.document.prefix);
      if (this.findLastBlock(tree.rootNode).type === 'comment') {
        intent = 'generation';
      } else {
        intent = 'completion';
      }

      log.debug(`TreeSitterParser: Detected intent ${intent} using tree-sitter with context.`);
    }

    return intent;
  }

  private async findGrammar(filename: string): Promise<Parser | undefined> {
    const grammar = this.languages.find((language) =>
      language.extensions.find((ext) => filename.endsWith(ext)),
    );
    if (!grammar) {
      return;
    }
    if (this.parsers[grammar.name]) {
      return this.parsers[grammar.name];
    }

    try {
      const parser = new Parser();
      const language = await Parser.Language.load(grammar.wasmPath);
      parser.setLanguage(language);
      this.parsers[grammar.name] = parser;
      log.debug(
        `TreeSitterParser: Loaded tree-sitter grammar (tree-sitter-${grammar.name}.wasm present).`,
      );
      return parser;
    } catch (err) {
      // NOTE: We validate the below is not present in generation.test.ts integration test.
      //       Make sure to update the test appropriately if changing the error.
      log.warn(
        'TreeSitterParser: Unable to load tree-sitter grammar due to an unexpected error.',
        getErrorFromException(err),
      );
      return undefined;
    }
  }

  protected findLastBlock(node: Parser.SyntaxNode): Parser.SyntaxNode {
    if (node.children.length > 0) {
      // Find the last child that is not a missing node
      for (let i = node.children.length - 1; i >= 0; i--) {
        const child = node.children[i];
        if (!child.isMissing()) {
          return this.findLastBlock(child);
        }
      }
    }

    return node;
  }
}
