import { Connection } from 'vscode-languageserver';
import { createFakePartial } from './test_utils/create_fake_partial';
import {
  StreamWithId,
  StreamingCompletionParams,
  StreamingCompletionResponse,
  streamingCompletionHandler,
} from './streaming_completion_handler';
import { DocumentTransformer } from './documents';
import { ConfigProvider } from './config';
import { IGitLabAPI } from './api';
import { DEBOUNCE_INTERVAL_MS, waitMs } from './message_handler';

jest.mock('./message_handler');

describe('streamingCompletionHandler', () => {
  const testStreamId = '1';
  let mockConnection: Connection;
  let mockDocuments: DocumentTransformer;
  let mockConfigProvider: ConfigProvider;
  let mockAPI: IGitLabAPI;
  const handlerParams = createFakePartial<StreamingCompletionParams>({
    textDocument: {
      uri: 'mydocument',
    },
    position: {
      line: 0,
      character: 0,
    },
    id: testStreamId,
  });

  beforeEach(() => {
    jest.mocked(waitMs).mockResolvedValue(undefined);
    mockConnection = createFakePartial<Connection>({
      onNotification: jest.fn().mockReturnValue({
        dispose: () => {},
      }),
      sendNotification: jest.fn(),
    });

    mockDocuments = createFakePartial<DocumentTransformer>({
      getContext: jest.fn().mockReturnValue({
        prefix: 'test',
        suffix: 'test',
        filename: 'test.js',
      }),
    });
    mockConfigProvider = createFakePartial<ConfigProvider>({
      get: jest.fn().mockReturnValue({ workspaceFolders: [] }),
    });
    mockAPI = createFakePartial<IGitLabAPI>({
      getStreamingCodeSuggestions: jest.fn(),
    });
  });

  it('does not call the API when no context is passed in', async () => {
    mockDocuments.getContext = jest.fn().mockReturnValue(undefined);
    const handler = streamingCompletionHandler(
      mockConnection,
      mockDocuments,
      mockConfigProvider,
      mockAPI,
    );
    await handler(handlerParams);
    expect(mockAPI.getStreamingCodeSuggestions).not.toBeCalled();
  });

  it('should call the streaming API and send notifications to the client', async () => {
    mockAPI.getStreamingCodeSuggestions = jest.fn().mockImplementation(async function* () {});

    const handler = streamingCompletionHandler(
      mockConnection,
      mockDocuments,
      mockConfigProvider,
      mockAPI,
    );
    await handler(handlerParams);

    expect(mockAPI.getStreamingCodeSuggestions).toBeCalledTimes(1);
    expect(mockConnection.sendNotification).toBeCalledWith(StreamingCompletionResponse, {
      id: testStreamId,
      done: true,
    });
  });

  it('when multiple messages are send back, sends multiple notifications', async () => {
    mockAPI.getStreamingCodeSuggestions = jest.fn().mockImplementation(async function* () {
      yield 'one';
      yield 'two';
    });

    const handler = streamingCompletionHandler(
      mockConnection,
      mockDocuments,
      mockConfigProvider,
      mockAPI,
    );
    await handler(handlerParams);

    expect(mockConnection.sendNotification).toHaveBeenNthCalledWith(
      1,
      StreamingCompletionResponse,
      {
        id: testStreamId,
        completion: 'one',
        done: false,
      },
    );
    expect(mockConnection.sendNotification).toHaveBeenNthCalledWith(
      2,
      StreamingCompletionResponse,
      {
        id: testStreamId,
        completion: 'two',
        done: false,
      },
    );
    expect(mockConnection.sendNotification).toHaveBeenNthCalledWith(
      3,
      StreamingCompletionResponse,
      {
        id: testStreamId,
        done: true,
      },
    );
  });

  describe('cancellation', () => {
    let callback: (stream: StreamWithId) => void;

    beforeEach(() => {
      callback = () => {};
      mockConnection.onNotification = jest.fn().mockImplementation((_name, c) => {
        callback = c;
        return { dispose: () => {} };
      });
    });
    it('cancels streaming thanks to debouncing', async () => {
      jest.useFakeTimers();

      jest.mocked(waitMs).mockImplementation(jest.requireActual('./message_handler').waitMs);

      const handler = streamingCompletionHandler(
        mockConnection,
        mockDocuments,
        mockConfigProvider,
        mockAPI,
      );
      const promise = handler(handlerParams);
      if (callback) {
        callback({ id: testStreamId });
      }
      jest.advanceTimersByTime(DEBOUNCE_INTERVAL_MS + 1);
      await promise;
      expect(mockDocuments.getContext).not.toHaveBeenCalled();
    });
    it('cancels streaming when the cancel notification is sent', async () => {
      jest.useFakeTimers();
      mockAPI.getStreamingCodeSuggestions = jest.fn().mockImplementation(async function* () {
        yield 'one';
        await new Promise((resolve) => setTimeout(resolve, 0));
        yield 'two';
      });

      const handler = streamingCompletionHandler(
        mockConnection,
        mockDocuments,
        mockConfigProvider,
        mockAPI,
      );
      const promise = handler(handlerParams);

      if (callback) {
        callback({ id: testStreamId });
      }

      jest.runAllTimers();
      await promise;

      expect(mockConnection.sendNotification).not.toHaveBeenCalledWith(
        StreamingCompletionResponse,
        {
          id: testStreamId,
          completion: 'two',
          done: false,
        },
      );
      expect(mockConnection.sendNotification).toHaveBeenNthCalledWith(
        1,
        StreamingCompletionResponse,
        {
          id: testStreamId,
          done: true,
        },
      );
    });
  });
});
