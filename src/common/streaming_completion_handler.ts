import {
  CancellationTokenSource,
  Connection,
  InlineCompletionParams,
  NotificationType,
} from 'vscode-languageserver';
import { ConfigProvider } from './config';
import { DocumentTransformer } from './documents';
import { CodeSuggestionRequest, IGitLabAPI } from './api';
import { log, getErrorFromException } from './log';
import { DEBOUNCE_INTERVAL_MS, waitMs } from './message_handler';

export interface StreamWithId {
  /** stream ID, the client must ensure that the id is unique */
  id: string;
}
export type StreamingCompletionParams = InlineCompletionParams & StreamWithId;
export interface StreamingCompletionResponse {
  /** stream ID taken from the request, all stream responses for one request will have the request's stream ID */
  id: string;
  /** most up-to-date generated suggestion, each time LS receives a chunk from LLM, it adds it to this string -> the client doesn't have to join the stream */
  completion?: string;
  done: boolean;
}

export const STREAMING_COMPLETION_REQUEST_NOTIFICATION = 'streamingCompletionRequest';
export const STREAMING_COMPLETION_RESPONSE_NOTIFICATION = 'streamingCompletionResponse';
export const CANCEL_STREAMING_COMPLETION_NOTIFICATION = 'cancelStreaming';

export const StreamingCompletionRequest = new NotificationType<StreamingCompletionParams>(
  STREAMING_COMPLETION_REQUEST_NOTIFICATION,
);
export const StreamingCompletionResponse = new NotificationType<StreamingCompletionResponse>(
  STREAMING_COMPLETION_RESPONSE_NOTIFICATION,
);
export const CancelStreaming = new NotificationType<StreamWithId>(
  CANCEL_STREAMING_COMPLETION_NOTIFICATION,
);

export const streamingCompletionHandler = function (
  connection: Connection,
  documents: DocumentTransformer,
  configProvider: ConfigProvider,
  api: IGitLabAPI,
) {
  return async (params: StreamingCompletionParams) => {
    const cancellationTokenSource = new CancellationTokenSource();
    const disposeStopStreaming = connection.onNotification(CancelStreaming, (stream) => {
      if (stream.id === params.id) {
        cancellationTokenSource.cancel();
      }
    });
    const cancellationToken = cancellationTokenSource.token;
    const endStream = async () => {
      await connection.sendNotification(StreamingCompletionResponse, {
        id: params.id,
        done: true,
      });
    };

    // debouncing
    await waitMs(DEBOUNCE_INTERVAL_MS);
    if (cancellationToken.isCancellationRequested) {
      log.debug('debounce triggered for streaming');
      await endStream();
      return;
    }

    const context = documents.getContext(
      params.textDocument.uri,
      params.position,
      configProvider.get().workspaceFolders ?? [],
      undefined,
    );

    if (!context) {
      return;
    }

    const request: CodeSuggestionRequest = {
      prompt_version: 1,
      project_path: '',
      project_id: -1,
      current_file: {
        content_above_cursor: context.prefix,
        content_below_cursor: context.suffix,
        file_name: context.filename,
      },
      intent: 'generation',
      stream: true,
    };

    try {
      for await (const response of api.getStreamingCodeSuggestions(request)) {
        if (cancellationToken.isCancellationRequested) {
          break;
        }
        await connection.sendNotification(StreamingCompletionResponse, {
          id: params.id,
          completion: response,
          done: false,
        });
      }
    } catch (err) {
      log.error('Error streaming code suggestions.', getErrorFromException(err));
    } finally {
      await endStream();
      disposeStopStreaming.dispose();
      cancellationTokenSource.dispose();
    }
  };
};
