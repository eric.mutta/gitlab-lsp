import {
  InlineCompletionTriggerKind,
  SelectedCompletionInfo,
} from 'vscode-languageserver-protocol';
import { isInlineCompletionContext } from './type_utils';
import { createFakePartial } from '../test_utils/create_fake_partial';

describe('utils/type_utils', () => {
  describe('isInlineCompletionContext', () => {
    it.each`
      object                                                                       | result
      ${undefined}                                                                 | ${false}
      ${{ triggerKind: InlineCompletionTriggerKind.Automatic }}                    | ${false}
      ${{ selectedCompletionInfo: createFakePartial<SelectedCompletionInfo>({}) }} | ${true}
    `('returns $result when object is $object', ({ object, result }) => {
      expect(isInlineCompletionContext(object)).toBe(result);
    });
  });
});
