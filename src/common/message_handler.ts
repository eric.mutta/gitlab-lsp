import {
  CancellationToken,
  CompletionItem,
  CompletionItemKind,
  CompletionParams,
  Connection,
  Disposable,
  InitializeParams,
  InitializeResult,
  InlineCompletionItem,
  InlineCompletionList,
  InlineCompletionParams,
  Position,
  Range,
  TextDocumentIdentifier,
  TextDocumentSyncKind,
  WorkspaceFoldersChangeEvent,
} from 'vscode-languageserver';
import { CodeSuggestionResponseChoice, IGitLabAPI } from './api';
import { ConfigProvider, IConfig } from './config';
import {
  CODE_SUGGESTIONS_CATEGORY,
  IClientContext,
  SnowplowTracker,
  TRACKING_EVENTS,
} from './tracking/snowplow_tracker';
import { DocumentTransformer } from './documents';
import {
  isAtOrNearEndOfLine,
  shouldRejectCompletionWithSelectedCompletionTextMismatch,
} from './suggestion/suggestion_filter';
import {
  API_ERROR_NOTIFICATION,
  API_RECOVERY_NOTIFICATION,
  CircuitBreaker,
} from './circuit_breaker/circuit_breaker';
import { AiGatewayClient } from './ai_gateway';
import {
  SuggestionClientAiGateway,
  SuggestionClientMonolith,
  SuggestionClientRouter,
} from './suggestion_client';
import { FeatureFlags } from './feature_flags';
import { sanitizeRange } from './utils/sanitize_range';
import { SuggestionsCache } from './suggestion/suggestions_cache';
import { SuggestionClientPipeline, createTreeSitterMiddleware } from './suggestion_client';
import { log, getErrorFromException } from './log';
import { OptionalCompletionContext } from './utils/type_utils';
import { Intent, TreeSitterParser } from './tree_sitter';
import { ICompletionIntentRequest, ICompletionIntentResponse } from './custom_notifications';

export type ChangeConfigOptions = { settings: IConfig };

export type CustomInitializeParams = InitializeParams & {
  initializationOptions?: IClientContext;
};

export const TOKEN_CHECK_NOTIFICATION = '$/gitlab/token/check';

export const SUGGESTION_ACCEPTED_COMMAND = 'gitlab.ls.codeSuggestionAccepted';

export const DEBOUNCE_INTERVAL_MS = 300;

export interface MessageHandlerOptions {
  configProvider: ConfigProvider;
  api: IGitLabAPI;
  aiGateway: AiGatewayClient;
  tracker: SnowplowTracker;
  connection: Connection;
  documentTransformer: DocumentTransformer;
  treeSitterParser: TreeSitterParser;
}

export interface ITelemetryNotificationParams {
  category: 'code_suggestions';
  action: TRACKING_EVENTS;
  context: {
    trackingId: string;
  };
}

export const waitMs = (msToWait: number) => new Promise((resolve) => setTimeout(resolve, msToWait));

type SuggestionRouteType = 'monolith' | 'aiGateway';

export class MessageHandler {
  readonly #suggestionClientRouter: SuggestionClientRouter<SuggestionRouteType>;

  readonly #suggestionClientPipeline: SuggestionClientPipeline;

  #configProvider: ConfigProvider;

  #api: IGitLabAPI;

  #aiGateway: AiGatewayClient;

  #tracker: SnowplowTracker;

  #connection: Connection;

  #documents: DocumentTransformer;

  #circuitBreaker = new CircuitBreaker();

  #subscriptions: Disposable[] = [];

  #suggestionsCache: SuggestionsCache;

  #treeSitterParser: TreeSitterParser;

  constructor({
    configProvider,
    api,
    aiGateway,
    tracker,
    connection,
    documentTransformer,
    treeSitterParser,
  }: MessageHandlerOptions) {
    this.#configProvider = configProvider;
    this.#api = api;
    this.#aiGateway = aiGateway;
    this.#tracker = tracker;
    this.#connection = connection;
    this.#documents = documentTransformer;
    this.#suggestionsCache = new SuggestionsCache(this.#configProvider);
    this.#treeSitterParser = treeSitterParser;

    this.#suggestionClientRouter = new SuggestionClientRouter<SuggestionRouteType>(
      {
        monolith: new SuggestionClientMonolith(this.#api),
        aiGateway: new SuggestionClientAiGateway(this.#aiGateway),
      },
      'monolith',
    );
    this.#suggestionClientPipeline = new SuggestionClientPipeline([
      this.#suggestionClientRouter.asMiddleware(),
      createTreeSitterMiddleware(treeSitterParser),
    ]);

    this.#subscribeToCircuitBreakerEvents();
  }

  onInitializeHandler = (params: CustomInitializeParams): InitializeResult => {
    const { clientInfo, initializationOptions, workspaceFolders } = params;

    this.#configProvider.set('clientInfo', clientInfo);
    this.#configProvider.set('workspaceFolders', workspaceFolders);
    this.#configProvider.set('baseAssetsUrl', initializationOptions?.baseAssetsUrl);

    this.#api.setClientInfo(clientInfo);
    this.#aiGateway.setClientInfo(clientInfo);

    this.#tracker.setClientContext({
      ide: initializationOptions?.ide ?? clientInfo,
      extension: initializationOptions?.extension,
    });

    return {
      capabilities: {
        completionProvider: {
          resolveProvider: true,
        },
        inlineCompletionProvider: true,
        textDocumentSync: TextDocumentSyncKind.Full,
      },
    };
  };

  didChangeConfigurationHandler = async (
    { settings }: ChangeConfigOptions = { settings: {} },
  ): Promise<void> => {
    const config = this.#configProvider.get();
    const telemetry = { ...config.telemetry, ...settings.telemetry };
    const codeCompletion = { ...config.codeCompletion, ...settings.codeCompletion };
    const shouldUseAiGateway =
      settings.featureFlags?.[FeatureFlags.CodeSuggestionsClientDirectToGateway];

    this.#suggestionClientRouter.routeTo(shouldUseAiGateway ? 'aiGateway' : 'monolith');
    this.#configProvider.merge({ ...settings, telemetry, codeCompletion });

    await this.#api.configureApi(this.#configProvider.get());

    const { valid, reason, message } = await this.#api.checkToken(this.#configProvider.get().token);
    if (!valid) {
      this.#configProvider.set('token', undefined);

      log.warn(`Token is invalid. ${message}. Reason: ${reason}`);
      await this.#connection.sendNotification(TOKEN_CHECK_NOTIFICATION, {
        message,
        reason,
      });
    } else {
      log.info('Token is valid');
    }

    // Configure Snowplow tracking
    await this.#tracker.reconfigure({
      baseUrl: this.#configProvider.get().baseUrl,
      enabled: telemetry?.enabled,
      trackingUrl: telemetry?.trackingUrl,
      actions: telemetry?.actions,
    });
  };

  telemetryNotificationHandler = async ({
    category,
    action,
    context,
  }: ITelemetryNotificationParams) => {
    if (category === CODE_SUGGESTIONS_CATEGORY) {
      const { trackingId } = context;

      if (trackingId && this.#tracker.canClientTrackEvent(action)) {
        switch (action) {
          case TRACKING_EVENTS.ACCEPTED:
            this.#tracker.updateSuggestionState(trackingId, TRACKING_EVENTS.ACCEPTED);
            break;
          case TRACKING_EVENTS.REJECTED:
            this.#tracker.updateSuggestionState(trackingId, TRACKING_EVENTS.REJECTED);
            break;
          case TRACKING_EVENTS.CANCELLED:
            this.#tracker.updateSuggestionState(trackingId, TRACKING_EVENTS.CANCELLED);
            break;
          case TRACKING_EVENTS.SHOWN:
            this.#tracker.updateSuggestionState(trackingId, TRACKING_EVENTS.SHOWN);
            break;
          case TRACKING_EVENTS.NOT_PROVIDED:
            this.#tracker.updateSuggestionState(trackingId, TRACKING_EVENTS.NOT_PROVIDED);
            break;
          default:
            break;
        }
      }
    }
  };

  completionHandler = async (
    { textDocument, position, context }: CompletionParams,
    token: CancellationToken,
  ): Promise<CompletionItem[]> => {
    let codeSuggestions: CompletionItem[] = [];
    const uniqueTrackingId = SnowplowTracker.uniqueTrackingId();

    try {
      codeSuggestions = (
        await this.#getSuggestions(uniqueTrackingId, textDocument, position, token, context)
      ).map((choice, index) => ({
        label: `GitLab Suggestion ${index + 1}: ${choice.text}`,
        kind: CompletionItemKind.Text,
        insertText: choice.text,
        detail: choice.text,
        command: {
          title: 'Accept suggestion',
          command: SUGGESTION_ACCEPTED_COMMAND,
          arguments: [choice.uniqueTrackingId],
        },
        data: {
          index,
          trackingId: choice.uniqueTrackingId,
        },
      }));
    } catch (e) {
      this.#tracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.ERRORED);
      this.#circuitBreaker.error();
      log.error('Failed to get code suggestions!', getErrorFromException(e));
    }

    return codeSuggestions;
  };

  inlineCompletionHandler = async (
    params: InlineCompletionParams,
    token: CancellationToken,
  ): Promise<InlineCompletionList> => {
    const uniqueTrackingId: string = SnowplowTracker.uniqueTrackingId();
    const completionInfo = params.context.selectedCompletionInfo;
    let rangeDiff = 0;

    if (
      shouldRejectCompletionWithSelectedCompletionTextMismatch(
        params.context,
        this.#documents.get(params.textDocument.uri),
      )
    ) {
      return { items: [] };
    }

    if (completionInfo) {
      const range = sanitizeRange(completionInfo.range);
      rangeDiff = range.end.character - range.start.character;
    }

    try {
      const items: InlineCompletionItem[] = (
        await this.#getSuggestions(
          uniqueTrackingId,
          params.textDocument,
          params.position,
          token,
          params.context,
        )
      )?.map((choice) => ({
        insertText: completionInfo
          ? `${completionInfo.text.substring(rangeDiff)}${choice.text}`
          : choice.text,
        range: Range.create(params.position, params.position),
        command: {
          title: 'Accept suggestion',
          command: SUGGESTION_ACCEPTED_COMMAND,
          arguments: [choice.uniqueTrackingId],
        },
        data: {
          trackingId: choice.uniqueTrackingId,
        },
      }));

      return { items };
    } catch (e) {
      this.#tracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.ERRORED);
      this.#circuitBreaker.error();
      log.error('Failed to get code suggestions!', getErrorFromException(e));
      return { items: [] };
    }
  };

  getIntentHandler = async (
    params: ICompletionIntentRequest,
  ): Promise<ICompletionIntentResponse> => {
    let intent: Intent | undefined;

    try {
      const suggestionContext = this.#documents.getContext(
        params.documentUri,
        params.position,
        this.#configProvider.get().workspaceFolders ?? [],
        undefined,
      );

      if (suggestionContext) {
        intent = await this.#treeSitterParser.getIntent({ document: suggestionContext });
      }
    } catch (error) {
      log.error('Failed to detect completion intent', getErrorFromException(error));
    }

    return {
      intent: intent ?? 'completion',
    };
  };

  #trackShowIfNeeded(uniqueTrackingId: string) {
    if (!this.#tracker.canClientTrackEvent(TRACKING_EVENTS.SHOWN)) {
      /* If the Client can detect when the suggestion is shown in the IDE, it will notify the Server.
          Otherwise the server will assume that returned suggestions are shown and tracks the event */
      this.#tracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.SHOWN);
    }
  }

  async #getSuggestions(
    uniqueTrackingId: string,
    textDocument: TextDocumentIdentifier,
    position: Position,
    token: CancellationToken,
    completionContext: OptionalCompletionContext,
  ): Promise<CodeSuggestionResponseChoice[]> {
    const context = this.#documents.getContext(
      textDocument.uri,
      position,
      this.#configProvider.get().workspaceFolders ?? [],
      completionContext,
    );

    if (context === undefined) {
      return [];
    }

    const cachedSuggestions = this.#suggestionsCache.getCachedSuggestions({
      document: textDocument,
      position,
      context,
    });

    if (cachedSuggestions?.length) {
      const { uniqueTrackingId: cachedTrackingId } = cachedSuggestions[0];
      this.#tracker.setCodeSuggestionsContext(
        cachedTrackingId,
        context,
        this.#configProvider.get().clientInfo,
        'cache',
      );

      this.#tracker.updateSuggestionState(cachedTrackingId, TRACKING_EVENTS.LOADED);
      this.#trackShowIfNeeded(cachedTrackingId);

      return cachedSuggestions;
    }

    // debounce
    await waitMs(DEBOUNCE_INTERVAL_MS);
    if (token.isCancellationRequested) {
      return [];
    }

    log.info('Suggestion requested.');

    if (this.#circuitBreaker.isOpen()) {
      log.warn('Code suggestions were not requested as the circuit breaker is open.');
      return [];
    }

    if (!this.#configProvider.get().token) {
      return [];
    }
    // Do not send a suggestion if content is less than 10 characters
    const contentLength = context?.prefix?.length + context?.suffix?.length;
    if (contentLength < 10) {
      return [];
    }

    if (!isAtOrNearEndOfLine(context.suffix)) {
      return [];
    }

    // Creates the suggestion and tracks suggestion_requested
    this.#tracker.setCodeSuggestionsContext(
      uniqueTrackingId,
      context,
      this.#configProvider.get().clientInfo,
    );

    const suggestionsResponse = await this.#suggestionClientPipeline.getSuggestions({
      document: context,
      projectPath: this.#configProvider.get().projectPath,
    });

    this.#tracker.updateCodeSuggestionsContext(
      uniqueTrackingId,
      suggestionsResponse?.model,
      suggestionsResponse?.status,
    );

    if (suggestionsResponse?.error) {
      throw suggestionsResponse.error;
    }
    this.#tracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.LOADED);

    this.#circuitBreaker.close();

    const areSuggestionsNotProvided =
      !suggestionsResponse?.choices?.length ||
      suggestionsResponse?.choices.every(({ text }) => !text?.length);

    const choices = (suggestionsResponse?.choices || []).map((choice) => ({
      ...choice,
      uniqueTrackingId,
    }));

    this.#suggestionsCache.addToSuggestionCache({
      request: {
        document: textDocument,
        position,
        context,
      },
      suggestions: choices,
    });

    if (token.isCancellationRequested) {
      this.#tracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.CANCELLED);
      return [];
    }

    if (areSuggestionsNotProvided) {
      this.#tracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.NOT_PROVIDED);
      return [];
    }

    this.#trackShowIfNeeded(uniqueTrackingId);

    return choices;
  }

  didChangeWorkspaceFoldersHandler = (params: WorkspaceFoldersChangeEvent) => {
    const { added, removed } = params;

    const removedKeys = removed.map(({ name }) => name);

    const currentWorkspaceFolders = this.#configProvider.get().workspaceFolders || [];
    const afterRemoved = currentWorkspaceFolders.filter((f) => !removedKeys.includes(f.name));
    const afterAdded = [...afterRemoved, ...(added || [])];

    this.#configProvider.set('workspaceFolders', afterAdded);
  };

  onShutdownHandler = () => {
    this.#subscriptions.forEach((subscription) => subscription?.dispose());
  };

  #subscribeToCircuitBreakerEvents() {
    this.#subscriptions.push(
      this.#circuitBreaker.onOpen(() => this.#connection.sendNotification(API_ERROR_NOTIFICATION)),
    );
    this.#subscriptions.push(
      this.#circuitBreaker.onClose(() =>
        this.#connection.sendNotification(API_RECOVERY_NOTIFICATION),
      ),
    );
  }
}
