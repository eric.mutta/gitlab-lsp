import { Position, RequestType } from 'vscode-languageserver-protocol';
import { URI } from 'vscode-languageserver';
import { Intent } from './tree_sitter/parser';

export interface ICompletionIntentResponse {
  intent: Intent;
}
export interface ICompletionIntentRequest {
  documentUri: URI;
  position: Position;
}

export interface ICompletionIntentError {
  error: string;
}

export const CompletionIntentRequest = new RequestType<
  ICompletionIntentRequest,
  ICompletionIntentResponse,
  ICompletionIntentError
>('$gitlab/getCompletionIntent');
