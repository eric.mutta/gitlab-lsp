import { compare as semverCompare } from 'semver';

import { IClientInfo } from './config';
import { ICodeSuggestionModel } from './tracking/snowplow_tracker';
import { IFetch } from './fetch';
import { log, getErrorFromException } from './log';

export const GITLAB_API_BASE_URL = 'https://gitlab.com';

export interface IGitLabAPI {
  configureApi({ token, baseUrl }: { token?: string; baseUrl?: string }): Promise<void>;
  checkToken(token: string | undefined): Promise<TokenCheckResponse>;

  setClientInfo(clientInfo: IClientInfo | undefined): void;

  getCodeSuggestions(request: CodeSuggestionRequest): Promise<CodeSuggestionResponse | undefined>;
  getStreamingCodeSuggestions(
    request: CodeSuggestionRequest,
  ): AsyncGenerator<string, undefined, void>;
}

export interface CodeSuggestionRequest {
  prompt_version: number;
  project_path: string;
  model_provider?: string;
  project_id: number;
  current_file: CodeSuggestionRequestCurrentFile;
  intent?: 'completion' | 'generation';
  stream?: boolean;
}

export interface CodeSuggestionRequestCurrentFile {
  file_name: string;
  content_above_cursor: string;
  content_below_cursor: string;
}

export interface CodeSuggestionResponse {
  choices?: CodeSuggestionResponseChoice[];
  model?: ICodeSuggestionModel;
  status: number;
  error?: string;
}

export interface CodeSuggestionResponseChoice {
  text: string;
  uniqueTrackingId: string;
}

export interface CompletionToken {
  access_token: string;
  /* expires in number of seconds since `created_at` */
  expires_in: number;
  /* unix timestamp of the datetime of token creation */
  created_at: number;
}

export interface PersonalAccessToken {
  name: string;
  scopes: string[];
  active: boolean;
}

export interface OAuthToken {
  scope: string[];
}

export interface TokenCheckResponse {
  valid: boolean;
  reason?: 'unknown' | 'not_active' | 'invalid_scopes';
  message?: string;
}

export class GitLabAPI implements IGitLabAPI {
  #completionToken: CompletionToken | undefined;
  #token: string | undefined;
  #baseURL: string;
  #clientInfo: IClientInfo | undefined;
  #useNewApiVersion: boolean = false;
  #lsFetch: IFetch;

  constructor(lsFetch: IFetch, baseURL = GITLAB_API_BASE_URL, token?: string) {
    this.#token = token;
    this.#baseURL = baseURL;
    this.#lsFetch = lsFetch;
  }

  async configureApi({
    token,
    baseUrl = GITLAB_API_BASE_URL,
  }: {
    token?: string;
    baseUrl?: string;
  }) {
    this.#token = token;
    this.#baseURL = baseUrl;

    await this.#setCodeSuggestionsApiVersion();
  }

  #looksLikePatToken(token: string): boolean {
    // OAuth tokens will be longer than 42 characters and PATs will be shorter.
    return token.length < 42;
  }

  async #checkPatToken(token: string): Promise<TokenCheckResponse> {
    const headers = this.#getDefaultHeaders(token);

    const response = await this.#lsFetch.get(
      `${this.#baseURL}/api/v4/personal_access_tokens/self`,
      { headers: headers },
    );

    if (response.ok) {
      const { active, scopes } = (await response.json()) as PersonalAccessToken;

      if (!active) {
        return {
          valid: false,
          reason: 'not_active',
          message: 'Token is not active.',
        };
      }

      if (!this.#hasValidScopes(scopes)) {
        const joinedScopes = scopes.map((scope) => `'${scope}'`).join(', ');

        return {
          valid: false,
          reason: 'invalid_scopes',
          message: `Token has scope(s) ${joinedScopes} (needs 'api').`,
        };
      }

      return { valid: true };
    } else {
      throw new Error(response.statusText);
    }
  }

  async #checkOAuthToken(token: string): Promise<TokenCheckResponse> {
    const headers = this.#getDefaultHeaders(token);

    const response = await this.#lsFetch.get(`${this.#baseURL}/oauth/token/info`, {
      headers,
    });

    if (response.ok) {
      const { scope: scopes } = (await response.json()) as OAuthToken;
      if (!this.#hasValidScopes(scopes)) {
        const joinedScopes = scopes.map((scope) => `'${scope}'`).join(', ');

        return {
          valid: false,
          reason: 'invalid_scopes',
          message: `Token has scope(s) ${joinedScopes} (needs 'api').`,
        };
      }

      return { valid: true };
    } else {
      throw new Error(response.statusText);
    }
  }

  async checkToken(token: string = ''): Promise<TokenCheckResponse> {
    try {
      if (this.#looksLikePatToken(token)) {
        log.info('Checking token for PAT validity');
        return await this.#checkPatToken(token);
      } else {
        log.info('Checking token for OAuth validity');
        return await this.#checkOAuthToken(token);
      }
    } catch (err) {
      log.error('Error performing token check', getErrorFromException(err));
      return {
        valid: false,
        reason: 'unknown',
        message: `Failed to check token: ${err}`,
      };
    }
  }

  #hasValidScopes(scopes: string[]): boolean {
    return scopes.includes('api');
  }
  setClientInfo(clientInfo: IClientInfo) {
    this.#clientInfo = clientInfo;
  }
  async #getGitLabInstanceVersion(): Promise<string> {
    if (!this.#token) {
      throw 'Token needs to be provided to retrieve GitLab version';
    }

    const headers = this.#getDefaultHeaders(this.#token);

    const response = await this.#lsFetch.get(`${this.#baseURL}/api/v4/version`, {
      headers,
    });
    const { version } = await response.json();

    return version;
  }

  async #setCodeSuggestionsApiVersion() {
    if (this.#baseURL?.endsWith(GITLAB_API_BASE_URL)) {
      this.#useNewApiVersion = true;
      return;
    }

    try {
      const version = await this.#getGitLabInstanceVersion();
      this.#useNewApiVersion = semverCompare(version, 'v16.3.0') >= 0;
    } catch (err) {
      log.error(`Failed to get GitLab version: ${err}. Defaulting to the old API`);
    }
  }

  async #getCodeSuggestionsV1(requestData: CodeSuggestionRequest): Promise<CodeSuggestionResponse> {
    const completionToken = await this.#getCompletionToken();

    const headers = {
      ...this.#getDefaultHeaders(completionToken),
      'Content-Type': 'application/json',
      'X-Gitlab-Authentication-Type': 'oidc',
    };

    const response = await this.#lsFetch.post('https://codesuggestions.gitlab.com/v2/completions', {
      headers,
      body: JSON.stringify(requestData),
    });

    const data = await response.json();

    return { ...data, status: response.status };
  }

  async #getCodeSuggestionsV2(requestData: CodeSuggestionRequest): Promise<CodeSuggestionResponse> {
    if (!this.#token) {
      throw 'Token needs to be provided to request Code Suggestions';
    }

    const headers = {
      ...this.#getDefaultHeaders(this.#token),
      'Content-Type': 'application/json',
    };

    const response = await this.#lsFetch.post(
      `${this.#baseURL}/api/v4/code_suggestions/completions`,
      { headers, body: JSON.stringify(requestData) },
    );

    const data = await response.json();
    return { ...data, status: response.status };
  }

  async getCodeSuggestions(
    request: CodeSuggestionRequest,
  ): Promise<CodeSuggestionResponse | undefined> {
    if (this.#useNewApiVersion) {
      return this.#getCodeSuggestionsV2(request);
    }
    return this.#getCodeSuggestionsV1(request);
  }

  async *getStreamingCodeSuggestions(
    request: CodeSuggestionRequest,
  ): AsyncGenerator<string, undefined, void> {
    if (!this.#token) {
      throw 'Token needs to be provided to stream code suggestions';
    }

    const headers = {
      ...this.#getDefaultHeaders(this.#token),
      'Content-Type': 'application/json',
    };

    let response: Response;

    try {
      response = await this.#lsFetch.fetchBase(
        `${this.#baseURL}/api/v4/code_suggestions/completions`,
        {
          method: 'POST',
          headers,
          body: JSON.stringify(request),
        },
      );

      if (!response.body) {
        return undefined;
      }

      if (response.status !== 200) {
        const body = await response.text();
        throw `HTTP error! Status: ${response.status}, Body = ${body}`;
      }
    } catch (e) {
      throw `Error in calling GitLab ${e}`;
    }

    const reader = response?.body?.getReader();
    if (!reader) {
      return undefined;
    }

    let buffer = '';
    let readResult = await reader.read();

    while (!readResult.done) {
      // TODO: We should consider streaming the raw bytes instead of the decoded string
      const rawContent = new TextDecoder().decode(readResult.value);
      buffer += rawContent;
      // TODO if we cancel the stream, and nothing will consume it, we probably leave the HTTP connection open :-O
      yield buffer;

      readResult = await reader.read();
    }

    return undefined;
  }

  async #getCompletionToken(): Promise<string> {
    // Check if token has expired
    if (this.#completionToken) {
      const unixTimestampNow = Math.floor(new Date().getTime() / 1000);
      if (unixTimestampNow < this.#completionToken.created_at + this.#completionToken.expires_in) {
        return this.#completionToken.access_token;
      }
    }

    if (!this.#token) {
      throw 'Token needs to be provided to get completion token';
    }

    const headers = this.#getDefaultHeaders(this.#token);

    const response = await this.#lsFetch.post(`${this.#baseURL}/api/v4/code_suggestions/tokens`, {
      headers,
    });

    this.#completionToken = (await response.json()) as CompletionToken;
    return this.#completionToken.access_token;
  }

  #getDefaultHeaders(token: string) {
    return {
      Authorization: `Bearer ${token}`,
      'User-Agent': `code-completions-language-server-experiment (${this.#clientInfo?.name}:${this
        .#clientInfo?.version})`,
    };
  }
}
