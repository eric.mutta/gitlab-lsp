import { AiGatewayClient, CompletionsRequest, GenerationsRequest } from '../ai_gateway';
import { SuggestionClient, SuggestionContext, SuggestionResponse } from './suggestion_client';

interface CompletionsTask {
  type: 'completions';
  request: CompletionsRequest;
}

interface GenerationsTask {
  type: 'generations';
  request: GenerationsRequest;
}

type SuggestionTask = CompletionsTask | GenerationsTask;

export class SuggestionClientAiGateway implements SuggestionClient {
  readonly #aiGateway: AiGatewayClient;

  constructor(aiGateway: AiGatewayClient) {
    this.#aiGateway = aiGateway;
  }

  async getSuggestions(context: SuggestionContext): Promise<SuggestionResponse | undefined> {
    const task = this.#createTask(context);

    if (task.type === 'completions') {
      return this.#aiGateway.fetchCompletions(task.request);
    } else {
      return this.#aiGateway.fetchGenerations(task.request);
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  #createTask(_context: SuggestionContext): SuggestionTask {
    // TODO: We need to move over the task building logic from Rails
    //       https://gitlab.com/gitlab-org/gitlab/-/blob/9244902f3e933c1ff3437478cf579f42ac285cad/ee/lib/code_suggestions/task_factory.rb#L20
    //       https://gitlab.com/gitlab-org/gitlab/-/issues/433433
    return {
      type: 'completions',
      request: {},
    };
  }
}
