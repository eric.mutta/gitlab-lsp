import { AiGatewayClient } from '../ai_gateway';
import { IDocContext } from '../documents';
import { createFakePartial } from '../test_utils/create_fake_partial';
import { SuggestionContext } from './suggestion_client';
import { SuggestionClientAiGateway } from './suggestion_client_ai_gateway';

const TEST_CONTEXT: SuggestionContext = {
  document: createFakePartial<IDocContext>({
    filename: 'test.md',
  }),
};

describe('SuggestionClientAiGateway', () => {
  let aiGateway: AiGatewayClient;
  let subject: SuggestionClientAiGateway;

  beforeEach(() => {
    aiGateway = createFakePartial<AiGatewayClient>({
      fetchCompletions: jest.fn(),
      fetchGenerations: jest.fn(),
    });

    subject = new SuggestionClientAiGateway(aiGateway);
  });

  describe('getSuggestions', () => {
    it('just requests token for now', async () => {
      const response = await subject.getSuggestions(TEST_CONTEXT);

      expect(aiGateway.fetchCompletions).toHaveBeenCalledWith({});
      expect(aiGateway.fetchGenerations).not.toHaveBeenCalled();
      expect(response).toBeUndefined();
    });
  });
});
