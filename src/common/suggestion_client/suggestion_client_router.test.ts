import { IDocContext } from '../documents';
import { createFakePartial } from '../test_utils/create_fake_partial';
import { SuggestionClient, SuggestionContext } from './suggestion_client';
import { SuggestionClientRouter } from './suggestion_client_router';

const TEST_CONTEXT: SuggestionContext = {
  document: createFakePartial<IDocContext>({
    filename: 'test.md',
  }),
};

type TestRoutes = 'lorem' | 'ipsum' | 'dolar';

describe('SuggestionClientRouter', () => {
  let clients: Record<TestRoutes, SuggestionClient>;
  let subject: SuggestionClientRouter<TestRoutes>;

  const createTestSuggestionClient = () => ({
    getSuggestions: jest.fn(),
  });

  beforeEach(() => {
    clients = {
      lorem: createTestSuggestionClient(),
      ipsum: createTestSuggestionClient(),
      dolar: createTestSuggestionClient(),
    };

    subject = new SuggestionClientRouter<TestRoutes>(clients, 'lorem');
  });

  describe('default', () => {
    it('only calls initial route', async () => {
      await subject.getSuggestions(TEST_CONTEXT);

      expect(clients.lorem.getSuggestions).toHaveBeenCalledWith(TEST_CONTEXT);
      expect(clients.ipsum.getSuggestions).not.toHaveBeenCalled();
      expect(clients.dolar.getSuggestions).not.toHaveBeenCalled();
    });
  });

  describe.each<TestRoutes>(['lorem', 'ipsum', 'dolar'])('when route changes to %s', (route) => {
    it('only calls the new route', async () => {
      await subject.routeTo(route);
      await subject.getSuggestions(TEST_CONTEXT);

      const { [route]: selectedClient, ...rest } = clients;

      expect(selectedClient.getSuggestions).toHaveBeenCalledWith(TEST_CONTEXT);
      Object.values<SuggestionClient>(rest).forEach((otherClient) => {
        expect(otherClient.getSuggestions).not.toHaveBeenCalled();
      });
    });
  });

  describe('asMiddleware', () => {
    it('returns function that calls current client', async () => {
      const middleware = subject.asMiddleware();
      const next = jest.fn();

      subject.routeTo('ipsum');

      await middleware(TEST_CONTEXT, next);

      expect(next).not.toHaveBeenCalled();
      expect(clients.ipsum.getSuggestions).toHaveBeenCalled();
    });
  });
});
