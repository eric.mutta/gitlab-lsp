import { IDocContext } from '../documents';

export interface SuggestionModel {
  lang: string;
  engine: string;
  name: string;
}

export interface SuggestionContext {
  document: IDocContext;
  intent?: 'completion' | 'generation';
  projectPath?: string;
}

export interface SuggestionResponse {
  choices?: SuggestionResponseChoice[];
  model?: SuggestionModel;
  status: number;
  error?: string;
}

export interface SuggestionResponseChoice {
  text: string;
}

export interface SuggestionClient {
  getSuggestions(context: SuggestionContext): Promise<SuggestionResponse | undefined>;
}

export type SuggestionClientFn = SuggestionClient['getSuggestions'];

export type SuggestionClientMiddleware = (
  context: SuggestionContext,
  next: SuggestionClientFn,
) => ReturnType<SuggestionClientFn>;
