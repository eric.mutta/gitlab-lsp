import { CodeSuggestionResponse, IGitLabAPI } from '../api';
import { IDocContext } from '../documents';
import { createFakePartial } from '../test_utils/create_fake_partial';
import { SuggestionContext } from './suggestion_client';
import { SuggestionClientMonolith } from './suggestion_client_monolith';

const TEST_SUGGESTION_RESPONSE = createFakePartial<CodeSuggestionResponse>({
  choices: [],
});
const TEST_CONTEXT: SuggestionContext = {
  document: createFakePartial<IDocContext>({
    filename: 'test.md',
    prefix: 'prefix-content',
    suffix: 'suffix-content',
  }),
  projectPath: 'gitlab-org/editor-extensions/gitlab-lsp',
};

describe('SuggestionClientMonolith', () => {
  let api: IGitLabAPI;
  let subject: SuggestionClientMonolith;

  beforeEach(() => {
    api = createFakePartial<IGitLabAPI>({
      getCodeSuggestions: jest.fn().mockResolvedValue(TEST_SUGGESTION_RESPONSE),
    });

    subject = new SuggestionClientMonolith(api);
  });

  describe('getSuggestions', () => {
    it('passes through to monolith api', async () => {
      const result = await subject.getSuggestions(TEST_CONTEXT);

      expect(result).toBe(TEST_SUGGESTION_RESPONSE);
      expect(api.getCodeSuggestions).toHaveBeenCalledWith({
        prompt_version: 1,
        project_path: TEST_CONTEXT.projectPath,
        project_id: -1,
        current_file: {
          content_above_cursor: 'prefix-content',
          content_below_cursor: 'suffix-content',
          file_name: 'test.md',
        },
      });
    });

    it('passes through intent if provided', async () => {
      await subject.getSuggestions({
        ...TEST_CONTEXT,
        intent: 'completion',
      });

      expect(api.getCodeSuggestions).toHaveBeenCalledWith(
        expect.objectContaining({
          intent: 'completion',
        }),
      );
    });
  });
});
