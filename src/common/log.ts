import dayjs from 'dayjs';
import { ConfigProvider } from './config';

export interface ILog {
  debug(e: Error): void;
  debug(message: string, e?: Error): void;
  info(e: Error): void;
  info(message: string, e?: Error): void;
  warn(e: Error): void;
  warn(message: string, e?: Error): void;
  error(e: Error): void;
  error(message: string, e?: Error): void;
}

export const LOG_LEVEL = {
  DEBUG: 'debug',
  INFO: 'info',
  WARNING: 'warning',
  ERROR: 'error',
} as const;

export type LogLevel = (typeof LOG_LEVEL)[keyof typeof LOG_LEVEL];

const LOG_LEVEL_MAPPING = {
  [LOG_LEVEL.ERROR]: 1,
  [LOG_LEVEL.WARNING]: 2,
  [LOG_LEVEL.INFO]: 3,
  [LOG_LEVEL.DEBUG]: 4,
};

const getNumericMapping = (logLevel: keyof typeof LOG_LEVEL_MAPPING | undefined) => {
  if (logLevel && logLevel in LOG_LEVEL_MAPPING) {
    return LOG_LEVEL_MAPPING[logLevel];
  } else {
    // Log level could be set as an invalid string by an LS client, or the .
    return LOG_LEVEL_MAPPING[LOG_LEVEL.INFO];
  }
};

// pad subsequent lines by 4 spaces
const PADDING = 4;

const multilineLog = (line: string, level: LogLevel): void => {
  const prefix = `${dayjs().format('YYYY-MM-DDTHH:mm:ss:SSS')} [${level}]: `;
  const padNextLines = (text: string) => text.replace(/\n/g, `\n${' '.repeat(PADDING)}`);

  console.log(`${prefix}${padNextLines(line)}`);
};

const formatError = (e: Error): string => `${e.message}\n${e.stack}`;

export const getErrorFromException = (e: unknown): Error | undefined => {
  let error;
  if (e instanceof Error) {
    error = e;
  } else if (typeof e === 'string') {
    error = new Error(e);
  }
  return error;
};

const logWithLevel = (level: LogLevel, a1: Error | string, a2?: Error) => {
  if (typeof a1 === 'string') {
    const errorText = a2 ? `\n${formatError(a2)}` : '';
    multilineLog(`${a1}${errorText}`, level);
  } else {
    multilineLog(formatError(a1), level);
  }
};

class Log implements ILog {
  #configProvider: ConfigProvider | undefined;

  setup(configProvider: ConfigProvider) {
    this.#configProvider = configProvider;
  }

  #log(incomingLogLevel: LogLevel, a1: Error | string, a2?: Error) {
    const configuredLevel = this.#configProvider?.get().logLevel;
    const shouldShowLog = getNumericMapping(configuredLevel) >= LOG_LEVEL_MAPPING[incomingLogLevel];
    if (shouldShowLog) {
      logWithLevel(incomingLogLevel, a1, a2);
    }
  }

  debug(a1: Error | string, a2?: Error) {
    this.#log(LOG_LEVEL.DEBUG, a1, a2);
  }
  info(a1: Error | string, a2?: Error) {
    this.#log(LOG_LEVEL.INFO, a1, a2);
  }
  warn(a1: Error | string, a2?: Error) {
    this.#log(LOG_LEVEL.WARNING, a1, a2);
  }
  error(a1: Error | string, a2?: Error) {
    this.#log(LOG_LEVEL.ERROR, a1, a2);
  }
}

// TODO: rename the export and replace usages of `log` with `Log`.
export const log = new Log();
