import * as fs from 'fs';
import * as path from 'path';

const wasmEntryPoints = () => {
  let files = [{ in: 'node_modules/web-tree-sitter/tree-sitter.wasm', out: 'node/tree-sitter' }];

  fs.readdirSync('vendor/grammars').forEach((file) => {
    if (file.endsWith('.wasm')) {
      files = [
        ...files,
        { in: `vendor/grammars/${file}`, out: `vendor/grammars/${file.replace(/\.wasm$/, '')}` },
      ];
    }
  });

  return files;
};

const fixVendorGrammarsPlugin = {
  name: 'fixVendorGrammars',
  setup(build) {
    build.onLoad({ filter: /.*\/tree_sitter\/languages\.ts/ }, ({ path: filePath }) => {
      if (!filePath.match(/.*\/?node_modules\/.*?/)) {
        let contents = fs.readFileSync(filePath, 'utf8');
        contents = contents.replaceAll(
          "path.join(__dirname, '../../../vendor/grammars",
          "path.join(__dirname, '../vendor/grammars",
        );
        return {
          contents,
          loader: path.extname(filePath).substring(1),
        };
      }
    });
  },
};

export { fixVendorGrammarsPlugin, wasmEntryPoints };
